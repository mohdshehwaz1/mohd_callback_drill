
/*write a function that will return all cards that belong to a particular list based on the listID
 that is passed to it from the given data in cards.json. 
 Then pass control back to the code that called it by using a callback function.
*/

function allCardsOfListID(cards,id,callbackfun){
    setTimeout(() => {
        if(Object.keys(cards).length ==0 || id == undefined) {
            callbackfun([])
            return
        }
        for(key in cards) {
            
            if(key==id){
                callbackfun(cards[key])
                
                
            }
        }
    }, 2*1000);

}

module.exports = allCardsOfListID
