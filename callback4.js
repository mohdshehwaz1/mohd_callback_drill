/*Write a function that will use the previously written functions to get the following information.
 You do not need to pass control back to the code that called it.
 Get information from the Thanos boards
Get all the lists for the Thanos board
Get all cards for the Mind list simultaneously
 */
const boardInfo = require('./callback1');
const allListBelongToParticularBoard = require('./callback2');
const allcardsOfListId = require('./callback3');

const boards = require('./boards.json');
const lists = require('./lists.json');
const cards = require('./cards.json');

function allCardsFromMindList(boardName, listName,findBoardId,findListId) {
    let ThanosBoardID = findBoardId(boardName);
    let ThanosBoardList = findListId(listName);

    boardInfo( boards, ThanosBoardID, (id,name) => {
        console.log(`Id is the ${id} and name is ${name}` )
        allListBelongToParticularBoard( lists, ThanosBoardID, (allList) => {
            console.log( allList);
            allcardsOfListId( cards, ThanosBoardList, (allCards) => {
                console.log( allCards);
            });
        });
    });
}




module.exports =  allCardsFromMindList
