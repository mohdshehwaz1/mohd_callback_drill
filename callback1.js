/*Write a function that will return a particular board's information based on the boardID 
that is passed from the given list of boards in boards.
json and then pass control back to the code that called it by using a callback function.
*/

const boardInfoOfParticularID=(boards,id,callbackfun) =>{
    

    
    setTimeout(() => {
        if(boards.length ==0 || id==undefined) {
            callbackfun(undefined,undefined)
            return
        }

        for(index=0;index<boards.length;index++) {
            if(boards[index]['id']==id) {
                callbackfun(id,boards[index]['name'])
                
                
            }
        }
    }, 2*1000);  
}


module.exports = boardInfoOfParticularID