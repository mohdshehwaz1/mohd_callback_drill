/*Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/



const boardInfo = require('./callback1');
const allListBelongToParticularBoard = require('./callback2');
const allcardsOfListId = require('./callback3');

const boards = require('./boards.json');
const lists = require('./lists.json');
const cards = require('./cards.json');

function allCardsOfAllList(boardName,findBoardId) {
    let ThanosBoardID = findBoardId(boardName);
    

    boardInfo( boards, ThanosBoardID, (id,name) => {
        console.log(`Id is the ${id} and name is ${name}` )
        allListBelongToParticularBoard( lists, ThanosBoardID, (allList) => {
            console.log(allList)
            const getallID=allList.map(index=>{
                return index.id
            })
            for(i=0;i<getallID.length;i++) {
                allcardsOfListId(cards,getallID[i],(cardsList) => {
                    console.log(cardsList)
                })
            }
            
        });
    });
}


module.exports = allCardsOfAllList