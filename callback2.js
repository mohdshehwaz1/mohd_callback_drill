/*
 Write a function that will return all lists that belong to a board based on the boardID 
 that is passed to it from the given data in lists.json. 
 Then pass control back to the code that called it by using a callback function.
*/

async function alllistBelongToBoardID(lists,id,callbackfun){
    
    setTimeout(() => {
        if(Object.keys(lists).length ==0 || id == undefined) {
            callbackfun({})
            return
        }
        for(key in lists) {
            
            if(key==id){
                callbackfun(lists[key])
                
                
            }
        }
    }, 2*1000);
}


module.exports = alllistBelongToBoardID